Welcome to the Black Poplar Experiement. This is just a simple experiment to
see if it's possible to grow black poplars from cuttings in a suburban garden
in West London with a view to plant them in suitable places in the borough at
a later date.

On this site, you will find news about the progress of black poplar cuttings
that were planted in winter 2019.
