---
title: "About the Experiment"
date: 2020-04-12T15:18:07+01:00
draft: false
---

The [black poplar](https://www.woodlandtrust.org.uk/trees-woods-and-wildlife/british-trees/a-z-of-british-trees/black-poplar/)
is native to Britain but is a declining species and is quite rare. The borough
of Ealing, in West London has a few black poplar trees and following a
discussion between members of the
[Hanwell and Norwood Green Orchard Trail](https://orchardtrail.wordpress.com/)
in autumn 2019, this experiment was started to try and grow trees in a local
garden with a few to possibly extend the experiment further if it proves
successful.
