---
title: "Observations"
date: 2020-04-26T14:46:55+01:00
draft: false
author: Bruno Girin
outputs:
- HTML
- JSON
- CSV
---

All the observations carried out during the experiment.
