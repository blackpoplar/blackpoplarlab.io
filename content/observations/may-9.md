---
title: "May 9"
date: 2020-05-09T10:40:43+01:00
draft: false
author: Bruno Girin
year: "2020"
month: "2020/05"
outputs:
- HTML
- JSON
- CSV
observations:
- id: 1
  height: 40
  leaves: 25
  status: full
  lushness: 4
- id: 2
  height: 23
  leaves: 15
  status: full
  lushness: 3
- id: 3
  height: 69
  leaves: 10
  status: partial
  lushness: 1
- id: 4
  height: 72
  leaves: 55
  status: full
  lushness: 4
- id: 5
  height: 52
  leaves: 25
  status: partial
  lushness: 2
- id: 6
  height: 63
  leaves: 50
  status: full
  lushness: 3
- id: 7
  height: 72
  leaves: 30
  status: partial
  lushness: 2
- id: 8
  height: 10
  leaves: 5
  status: full
  lushness: 2
- id: 9
  height: 66
  leaves: 15
  status: full
  lushness: 2
- id: 10
  height: 73
  leaves: 0
  status: none
  lushness: 0
- id: 11
  height: 62
  leaves: 35
  status: full
  lushness: 3
- id: 12
  height: 53
  leaves: 115
  status: full
  lushness: 5
- id: 13
  height: 52
  leaves: 60
  status: full
  lushness: 5
- id: 14
  height: 35
  leaves: 5
  status: quickening
  lushness: 1
- id: 15
  height: 38
  leaves: 0
  status: none
  lushness: 0
- id: 16
  height: 52
  leaves: 50
  status: full
  lushness: 5
- id: 17
  height: 20
  leaves: 15
  status: full
  lushness: 4
- id: 18
  height: 14
  leaves: 5
  status: full
  lushness: 3
- id: 19
  height: 30
  leaves: 0
  status: none
  lushness: 0
- id: 20
  height: 35
  leaves: 35
  status: full
  lushness: 4
- id: 21
  height: 35
  leaves: 20
  status: full
  lushness: 3
- id: 22
  height: 22
  leaves: 10
  status: full
  lushness: 2
- id: 23
  height: 20
  leaves: 20
  status: full
  lushness: 4
- id: 24
  height: 43
  leaves: 25
  status: full
  lushness: 3
- id: 25
  height: 29
  leaves: 10
  status: full
  lushness: 3
- id: 26
  height: 20
  leaves: 10
  status: full
  lushness: 3
- id: 27
  height: 14
  leaves: 10
  status: full
  lushness: 3
- id: 28
  height: 42
  leaves: 0
  status: none
  lushness: 0
- id: 29
  height: 50
  leaves: 35
  status: full
  lushness: 3
- id: 30
  height: 40
  leaves: 35
  status: full
  lushness: 4
---

Two new trees have leaves, a few of them have grown by a centimeter or two and
have put on more leaves. A few have lost leaves due to slug or caterpillar
dammage: this seems to be limited to leaves close to the ground and is fine
for the taller trees but potentially concerning for the shorter ones.
