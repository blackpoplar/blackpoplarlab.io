---
title: "Initial"
date: 2020-04-25T15:00:00+01:00
draft: false
author: Bruno Girin
year: "2020"
month: "2020/04"
outputs:
- HTML
- JSON
- CSV
observations:
- id: 1
  height: 40
  leaves: 20
  status: full
  lushness: 4
- id: 2
  height: 23
  leaves: 10
  status: full
  lushness: 4
- id: 3
  height: 69
  leaves: 0
  status: none
  lushness: 0
- id: 4
  height: 71
  leaves: 40
  status: full
  lushness: 3
- id: 5
  height: 51
  leaves: 0
  status: none
  lushness: 0
- id: 6
  height: 63
  leaves: 25
  status: partial
  lushness: 2
- id: 7
  height: 70
  leaves: 5
  status: quickening
  lushness: 1
- id: 8
  height: 10
  leaves: 5
  status: full
  lushness: 2
- id: 9
  height: 66
  leaves: 10
  status: partial
  lushness: 2
- id: 10
  height: 73
  leaves: 0
  status: none
  lushness: 0
- id: 11
  height: 58
  leaves: 20
  status: partial
  lushness: 2
- id: 12
  height: 53
  leaves: 115
  status: full
  lushness: 5
- id: 13
  height: 49
  leaves: 40
  status: full
  lushness: 5
- id: 14
  height: 34
  leaves: 0
  status: none
  lushness: 0
- id: 15
  height: 38
  leaves: 0
  status: none
  lushness: 0
- id: 16
  height: 50
  leaves: 45
  status: full
  lushness: 4
- id: 17
  height: 18
  leaves: 10
  status: full
  lushness: 4
- id: 18
  height: 14
  leaves: 5
  status: full
  lushness: 3
- id: 19
  height: 30
  leaves: 0
  status: none
  lushness: 0
- id: 20
  height: 35
  leaves: 25
  status: full
  lushness: 4
- id: 21
  height: 35
  leaves: 10
  status: partial
  lushness: 2
- id: 22
  height: 22
  leaves: 5
  status: full
  lushness: 2
- id: 23
  height: 19
  leaves: 15
  status: full
  lushness: 4
- id: 24
  height: 43
  leaves: 20
  status: full
  lushness: 3
- id: 25
  height: 29
  leaves: 20
  status: full
  lushness: 3
- id: 26
  height: 20
  leaves: 10
  status: full
  lushness: 3
- id: 27
  height: 13
  leaves: 5
  status: full
  lushness: 3
- id: 28
  height: 42
  leaves: 0
  status: none
  lushness: 0
- id: 29
  height: 49
  leaves: 20
  status: full
  lushness: 3
- id: 30
  height: 39
  leaves: 25
  status: full
  lushness: 4
---

This was the first time I recorded observations on the trees. It's a bit late
in the process as it should have been done as soon as the cuttings were
planted. Better late than never!
