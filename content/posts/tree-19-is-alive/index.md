---
title: "Tree 19 Is Alive!"
date: 2020-05-13T18:48:37+01:00
draft: false
author: Bruno Girin
year: "2020"
month: "2020/05"
categories:
- News
tags:
- poplars
image: 20200513_184116-header.jpg
---

Tree 19 is alive!

<!--more-->

{{< postimage "20200513_184116.jpg" "Tree Number 19" >}}
