---
title: "2 New Trees"
date: 2020-05-06T18:04:54+01:00
draft: false
author: Bruno Girin
year: "2020"
month: "2020/05"
categories:
- News
tags:
- poplars
image: 20200504_105426-header.jpg
---

It looks like trees 3 and 14 are about to quicken and bring out leaves.

<!--more-->

{{< postimage "20200504_105426.jpg" "Tree Number 3" >}}

{{< postimage "20200505_180504.jpg" "Tree Number 14" >}}
