---
title: "Tree 10 Is Alive!"
date: 2020-05-21T19:19:13+01:00
draft: false
author: Bruno Girin
year: "2020"
month: "2020/05"
categories:
- News
tags:
- poplars
image: 20200520_191624-header.jpg
---

Tree 10 is alive!

<!--more-->

{{< postimage "20200520_191624.jpg" "Tree Number 10" >}}

