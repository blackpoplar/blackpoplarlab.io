---
title: "Welcome"
date: 2020-04-20T16:06:13+01:00
draft: false
author: Bruno Girin
year: "2020"
month: "2020/04"
categories:
- News
tags:
- welcome
---

Welcome to the Black Poplar Experiment! This section is where all the key news
about the experiment will be shared.
