---
title: "Cuttings Layout"
date: 2020-04-21T15:40:31+01:00
draft: false
author: Bruno Girin
year: "2020"
month: "2020/04"
categories:
- News
tags:
- layout
---

The different cuttings were planted in raised beds, pots and flower beds in
my garden. Here is the overall layout.

<!--more-->

The numbers in the diagram represent individual cuttings and will be used to
uniquely identify a tree throughout the experiment. Some cuttings are quite
large, others are much smaller. Branches that had many offshoots were cut to
generate more individual cuttings.

{{< readsvg "layout.svg" >}}

The cuttings were separated in several sections:
* Cuttings 1 to 13 are in raised beds. Cuttings 2 and 8 are offshoots of
  1 and 7 respectively that broke off after the main planting and rather
  than discard them, I planted them next to the main branch they came from.
* Cuttings 14 to 27 are in pots. They tend to be smaller and the size of
  the pot is also roughly aligned with how big the original cutting was.
* Cuttings 28 to 30 were planted directly in a flower bed that also has
  a couple of fruit trees, some lavender and a variety of other plants.
