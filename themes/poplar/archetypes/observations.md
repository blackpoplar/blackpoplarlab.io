---
title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date }}
draft: false
author: Bruno Girin
year: "{{ dateFormat "2006" .Date }}"
month: "{{ dateFormat "2006/01" .Date }}"
outputs:
- HTML
- JSON
- CSV
observations: []
---

Commentary on the observations goes here
