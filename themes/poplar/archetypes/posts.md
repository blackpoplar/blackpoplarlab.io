---
title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date }}
draft: false
author: Bruno Girin
year: "{{ dateFormat "2006" .Date }}"
month: "{{ dateFormat "2006/01" .Date }}"
categories:
- News
tags:
- poplars
---

The summary goes here

<!--more-->

And the main content goes here
